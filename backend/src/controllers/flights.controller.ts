import { JsonController, Get, Put, Param, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService();

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('/status/:id')
    async updateStatus(@Param('id') id: number, @Body() body: any) {
        const { status } = body;
        console.log(status);

    }
}
